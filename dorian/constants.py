G_cgs = 6.674e-8  # gravitational constant in cgs
c_cgs = 2.998e10  # speed of light in cgs
M_sun_cgs = 1.989e33  # solar mass in cgs
Mpc2cm = 3.086e24  # from Megaparsec to centimeters
Mpc2km = 3.086e19  # from Megaparsec to centimeters
